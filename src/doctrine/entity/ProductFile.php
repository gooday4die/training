<?php



/**
 * ProductFile
 */
class ProductFile
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $patch;

    /**
     * @var \Product
     */
    private $product;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ProductFile
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set patch
     *
     * @param string $patch
     *
     * @return ProductFile
     */
    public function setPatch($patch)
    {
        $this->patch = $patch;

        return $this;
    }

    /**
     * Get patch
     *
     * @return string
     */
    public function getPatch()
    {
        return $this->patch;
    }

    /**
     * Set product
     *
     * @param \Product $product
     *
     * @return ProductFile
     */
    public function setProduct(\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \Product
     */
    public function getProduct()
    {
        return $this->product;
    }
}
