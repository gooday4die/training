<?php
namespace app\Model\Product;

use App\Model\Base\Product\ProductFile as BaseProductFile;

class ProductFile extends BaseProductFile
{
    static $listFields = [];

    static $rules = [];

    public function scopeNoFilter($query)
    {
        return $query;
    }

    public function addProductFileFilter($query, $params)
    {
        /* EXAMPLE
        *    if (isset($params['name']) && !empty($params['name'])) {
        *       $query->where('name', 'LIKE', '%' . $params['name'] . '%');
        *    }
        */

        return $query;
    }

    public function scopeFilter($query, $params)
    {
        $query = ProductFile::noFilter($query);
        $query = ProductFile::addProductFileFilter($query, $params);

        return $query;
    }
}