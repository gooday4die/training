<?php
namespace app\Model\Base\User;

use Illuminate\Database\Eloquent\Model;





class User extends Model
{

    

    protected $table = 'user';

    protected $fillable = [
        'email',
        'password',
        'login',
    ];

        public function role()
    {
        return $this->belongsToMany('app\Model\Role\Role',  'user__role',  'user_id', 'id'); 
    }
    
}