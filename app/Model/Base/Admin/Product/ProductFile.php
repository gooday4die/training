<?php
namespace app\Model\Base\Admin\Product;

use Illuminate\Database\Eloquent\Model;





class ProductFile extends Model
{

    

    protected $table = 'product__file';

    protected $fillable = [
        'name',
        'patch',
        'product_id',
    ];

        public function product()
    {
        return $this->belongsTo('app\Model\Admin\Product\Product',  'id', 'product_id'); 
    }
    
}