<?php
namespace app\Model\Base\Admin\User;

use Illuminate\Database\Eloquent\Model;





class User extends Model
{

    

    protected $table = 'user';

    protected $fillable = [
        'email',
        'password',
        'login',
    ];

        public function role()
    {
        return $this->belongsToMany('app\Model\Admin\Role\Role',  'user__role',  'user_id', 'id'); 
    }
    
}