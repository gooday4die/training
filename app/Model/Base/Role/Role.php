<?php
namespace app\Model\Base\Role;

use Illuminate\Database\Eloquent\Model;





class Role extends Model
{

    

    protected $table = 'role';

    protected $fillable = [
        'name',
        'description',
    ];

        public function user()
    {
        return $this->belongsToMany('app\Model\User\User',  'user__role',  'role_id', 'id'); 
    }
    
}