<?php
namespace app\Model\Base\Product;

use Illuminate\Database\Eloquent\Model;





class Product extends Model
{

    

    protected $table = 'product';

    protected $fillable = [
        'name',
        'description',
        'price',
    ];

            
}