<?php
namespace app\Model\Base\Product;

use Illuminate\Database\Eloquent\Model;





class ProductFile extends Model
{

    

    protected $table = 'product__file';

    protected $fillable = [
        'name',
        'patch',
        'product_id',
    ];

        public function product()
    {
        return $this->belongsTo('app\Model\Product\Product',  'id', 'product_id'); 
    }
    
}