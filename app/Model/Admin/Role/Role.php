<?php
namespace app\Model\Admin\Role;

use App\Model\Base\Admin\Role\Role as BaseRole;

class Role extends BaseRole
{
    static $listFields = [];

    static $rules = [];

    public function scopeNoFilter($query)
    {
        return $query;
    }

    public function addRoleFilter($query, $params)
    {
        /* EXAMPLE
        *    if (isset($params['name']) && !empty($params['name'])) {
        *       $query->where('name', 'LIKE', '%' . $params['name'] . '%');
        *    }
        */

        return $query;
    }

    public function scopeFilter($query, $params)
    {
        $query = Role::noFilter($query);
        $query = Role::addRoleFilter($query, $params);

        return $query;
    }
}