<?php
Route::group([
    'prefix' => 'role',
    'as' => 'role.',
    'namespace' => 'Admin\Role',
    ], function () {

    Route::get('',             ['as' => 'index',  'uses' => 'RoleController@index']);

    Route::get('create',       ['as' => 'create', 'uses' => 'RoleController@create']);
    Route::post('save',        ['as' => 'save',   'uses' => 'RoleController@save']);

    Route::get('edit/{id}',    ['as' => 'edit',   'uses' => 'RoleController@edit']);
    Route::any('update/{id}',  ['as' => 'update', 'uses' => 'RoleController@update']);

    Route::any('delete/{id}',  ['as' => 'delete', 'uses' => 'RoleController@delete']);

});