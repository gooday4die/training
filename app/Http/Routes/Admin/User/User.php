<?php
Route::group([
    'prefix' => 'user',
    'as' => 'user.',
    'namespace' => 'Admin\User',
    ], function () {

    Route::get('',             ['as' => 'index',  'uses' => 'UserController@index']);

    Route::get('create',       ['as' => 'create', 'uses' => 'UserController@create']);
    Route::post('save',        ['as' => 'save',   'uses' => 'UserController@save']);

    Route::get('edit/{id}',    ['as' => 'edit',   'uses' => 'UserController@edit']);
    Route::any('update/{id}',  ['as' => 'update', 'uses' => 'UserController@update']);

    Route::any('delete/{id}',  ['as' => 'delete', 'uses' => 'UserController@delete']);

});