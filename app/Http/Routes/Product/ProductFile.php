<?php
Route::group([
    'prefix' => 'product__file',
    'as' => 'product__file.',
    'namespace' => 'Product',
    ], function () {

    Route::get('',             ['as' => 'index',  'uses' => 'ProductFileController@index']);

    Route::get('create',       ['as' => 'create', 'uses' => 'ProductFileController@create']);
    Route::post('save',        ['as' => 'save',   'uses' => 'ProductFileController@save']);

    Route::get('edit/{id}',    ['as' => 'edit',   'uses' => 'ProductFileController@edit']);
    Route::any('update/{id}',  ['as' => 'update', 'uses' => 'ProductFileController@update']);

    Route::any('delete/{id}',  ['as' => 'delete', 'uses' => 'ProductFileController@delete']);

});