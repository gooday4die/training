<?php

Route::group([
        'prefix' => 'product',
        'as' => 'product.',
        'namespace' => 'Product',

        ], function () {

        Route::get('',             ['as' => 'index',  'uses' => 'ProductController@index']);

        Route::get('create',       ['as' => 'create', 'uses' => 'ProductController@create']);
        Route::post('save',        ['as' => 'save',   'uses' => 'ProductController@save']);

        Route::get('edit/{id}',    ['as' => 'edit',   'uses' => 'ProductController@edit']);
        Route::any('update/{id}',  ['as' => 'update', 'uses' => 'ProductController@update']);

        Route::any('delete/{id}',  ['as' => 'delete', 'uses' => 'ProductController@delete']);

                                    include 'Routes/Product/ProductFile.php';
                            });

    
        Route::group([
        'prefix' => 'role',
        'as' => 'role.',
        'namespace' => 'Role',

        ], function () {

        Route::get('',             ['as' => 'index',  'uses' => 'RoleController@index']);

        Route::get('create',       ['as' => 'create', 'uses' => 'RoleController@create']);
        Route::post('save',        ['as' => 'save',   'uses' => 'RoleController@save']);

        Route::get('edit/{id}',    ['as' => 'edit',   'uses' => 'RoleController@edit']);
        Route::any('update/{id}',  ['as' => 'update', 'uses' => 'RoleController@update']);

        Route::any('delete/{id}',  ['as' => 'delete', 'uses' => 'RoleController@delete']);

                });

    
        Route::group([
        'prefix' => 'user',
        'as' => 'user.',
        'namespace' => 'User',

        ], function () {

        Route::get('',             ['as' => 'index',  'uses' => 'UserController@index']);

        Route::get('create',       ['as' => 'create', 'uses' => 'UserController@create']);
        Route::post('save',        ['as' => 'save',   'uses' => 'UserController@save']);

        Route::get('edit/{id}',    ['as' => 'edit',   'uses' => 'UserController@edit']);
        Route::any('update/{id}',  ['as' => 'update', 'uses' => 'UserController@update']);

        Route::any('delete/{id}',  ['as' => 'delete', 'uses' => 'UserController@delete']);

                });

    