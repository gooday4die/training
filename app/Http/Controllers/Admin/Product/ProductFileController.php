<?php
namespace App\Http\Controllers\Admin\Product;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Admin\Product\ProductFile;
use Illuminate\Http\Request;

class ProductFileController extends Controller {

    public function index()
    {
        $productFile = ProductFile::orderBy('id', 'desc')->paginate(10);

        return view('admin.product.product__file.index', [
            'productFile' => $productFile
        ]);
    }


    public function create()
    {
        return view('admin.product.product__file.create');
    }


    public function save(Request $request)
    {
        $productFile = new ProductFile($request->all());

        $productFile->save();

        return redirect()->route('productFile.index');
    }


    public function edit($id)
    {
        $productFile = ProductFile::find($id);

        return view('admin.product.product__file.edit', [
            'productFile' => $productFile
        ]);
    }


    public function update(Request $request, $id)
    {
        $productFile = ProductFile::find($id);

        $productFile->update($request->all());

        return redirect()->route('productFile.index');
    }


    public function delete($id)
    {
        $productFile = ProductFile::where('id', '=', $id);
        $productFile->delete();

        return redirect()->back();
    }

}