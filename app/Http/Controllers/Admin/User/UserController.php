<?php
namespace App\Http\Controllers\Admin\User;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Admin\User\User;
use Illuminate\Http\Request;

class UserController extends Controller {

    public function index()
    {
        $user = User::orderBy('id', 'desc')->paginate(10);

        return view('admin.user.user.index', [
            'user' => $user
        ]);
    }


    public function create()
    {
        return view('admin.user.user.create');
    }


    public function save(Request $request)
    {
        $user = new User($request->all());

        $user->save();

        return redirect()->route('user.index');
    }


    public function edit($id)
    {
        $user = User::find($id);

        return view('admin.user.user.edit', [
            'user' => $user
        ]);
    }


    public function update(Request $request, $id)
    {
        $user = User::find($id);

        $user->update($request->all());

        return redirect()->route('user.index');
    }


    public function delete($id)
    {
        $user = User::where('id', '=', $id);
        $user->delete();

        return redirect()->back();
    }

}