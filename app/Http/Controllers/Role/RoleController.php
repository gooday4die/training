<?php
namespace App\Http\Controllers\Role;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Role\Role;
use Illuminate\Http\Request;

class RoleController extends Controller {

    public function index()
    {
        $role = Role::orderBy('id', 'desc')->paginate(10);

        return view('role.role.index', [
            'role' => $role
        ]);
    }


    public function create()
    {
        return view('role.role.create');
    }


    public function save(Request $request)
    {
        $role = new Role($request->all());

        $role->save();

        return redirect()->route('role.index');
    }


    public function edit($id)
    {
        $role = Role::find($id);

        return view('role.role.edit', [
            'role' => $role
        ]);
    }


    public function update(Request $request, $id)
    {
        $role = Role::find($id);

        $role->update($request->all());

        return redirect()->route('role.index');
    }


    public function delete($id)
    {
        $role = Role::where('id', '=', $id);
        $role->delete();

        return redirect()->back();
    }

}