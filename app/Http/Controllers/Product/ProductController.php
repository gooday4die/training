<?php
namespace App\Http\Controllers\Product;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Product\Product;
use Illuminate\Http\Request;

class ProductController extends Controller {

    public function index()
    {
        $product = Product::orderBy('id', 'desc')->paginate(10);

        return view('product.product.index', [
            'product' => $product
        ]);
    }


    public function create()
    {
        return view('product.product.create');
    }


    public function save(Request $request)
    {
        $product = new Product($request->all());

        $product->save();

        return redirect()->route('product.index');
    }


    public function edit($id)
    {
        $product = Product::find($id);

        return view('product.product.edit', [
            'product' => $product
        ]);
    }


    public function update(Request $request, $id)
    {
        $product = Product::find($id);

        $product->update($request->all());

        return redirect()->route('product.index');
    }


    public function delete($id)
    {
        $product = Product::where('id', '=', $id);
        $product->delete();

        return redirect()->back();
    }

}