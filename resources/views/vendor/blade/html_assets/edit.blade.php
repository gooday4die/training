{% $doggy %}extends('layout')

{% $doggy %}section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-edit"></i> {% $class %} / Edit #{{ ${% $var %}->id }}</h1>
    </div>
{% $doggy %}endsection

{% $doggy %}section('content')
    {% $doggy %}include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('{% $var %}.update', ${% $var %}->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('{% $var %}.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
    </div>
{% $doggy %}endsection