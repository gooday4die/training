{% $doggy %}extends('layout')

{% $doggy %}section('header')
    <div class="page-header clearfix">
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i> {% $class %}
            <a class="btn btn-success pull-right" href="{{ route('{% $var %}.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
        </h1>
    </div>
{% $doggy %}endsection

{% $doggy %}section('content')
    <div class="row">
        <div class="col-md-12">
            {% $doggy %}if(${% $var %}->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        {% $doggy %}foreach(${% $var %} as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td class="text-right">
                                    <a class="btn btn-xs btn-warning" href="{{ route('{% $var %}.edit', $item->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                    <form action="{{ route('{% $var %}.delete', $item->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                        {% $doggy %}endforeach
                    </tbody>
                </table>
                {!! ${% $var %}->render() !!}
            {% $doggy %}else
                <h3 class="text-center alert alert-info">Empty!</h3>
            {% $doggy %}endif

        </div>
    </div>

{% $doggy %}endsection