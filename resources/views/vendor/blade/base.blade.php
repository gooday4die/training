namespace {{ $namespace }};

use Illuminate\Database\Eloquent\Model;

{{ isset($softDeletes) ? "use Illuminate\Database\Eloquent\SoftDeletes;" : '' }}



class {{ $class }} extends Model
{

    {{ isset($softDeletes) ? "use SoftDeletes;" : '' }}

    protected $table = '{{ $table }}';

    protected $fillable = [
    @foreach($fillable as $field)
    '{{ $field }}',
    @endforeach
];

    <?php $relationships = isset($relationships) ? $relationships : array() ?>
    @forelse($relationships as $name => $rel)
public function {{ $rel['targetEntity'] }}()
    {
        return $this->{{ $rel['type'] }}('{{ $rel['path'] }}', @if (isset($rel['relation_table'])) '{{ $rel['relation_table'] }}', @endif '{{ $rel['foreign'] }}', '{{ $rel['local'] }}'); 
    }
    @empty
    @endforelse

}