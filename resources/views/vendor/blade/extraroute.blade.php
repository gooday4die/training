{{--
@if ( $masters )
    // Основные сущности
    'masters' =>
    [
    @foreach($masters as $master)
        '{{$master}}',
    @endforeach
    ],
@endif

@if  ($subordinates)
    // Подчененные сущности
    'subordinates' =>
    [
    @foreach($subordinates as $key=>$subordinate)
        '{{$key}}' =>
        [
        @foreach($subordinate as $s)
            '{{$s['name']}}',
        @endforeach
        ],
    @endforeach
    ],
@endif
@if ( $options )
    'options' =>
    [
    @foreach( $options as $key=>$value )
        '{{$key}}' => @if ( $value==1 ) {{$value}}, @else '{{$value}}', @endif

    @endforeach
    ],
@endif
--}}
@if ( isset($route_prefix) )
    Route::group([
    'prefix' => '{{ strtolower($route_prefix) }}',
    'as' => '{{ strtolower($route_prefix) }}.',
    ], function () {
@endif
@if (isset($groups))
    @foreach($groups as $key=>$property)

        Route::group([
        'prefix' => '{{ $property['prefix'] }}',
        'as' => '{{ $property['as'] }}',
        'namespace' => '{{ $property['namespace'] }}',

        ], function () {

        Route::get('',             ['as' => 'index',  'uses' => '{{ $property['controller'] }}@index']);

        Route::get('create',       ['as' => 'create', 'uses' => '{{ $property['controller'] }}@create']);
        Route::post('save',        ['as' => 'save',   'uses' => '{{ $property['controller'] }}@save']);

        Route::get('edit/{id}',    ['as' => 'edit',   'uses' => '{{ $property['controller'] }}@edit']);
        Route::any('update/{id}',  ['as' => 'update', 'uses' => '{{ $property['controller'] }}@update']);

        Route::any('delete/{id}',  ['as' => 'delete', 'uses' => '{{ $property['controller'] }}@delete']);

        @if (isset($property['subordinates']))
            @foreach($property['subordinates'] as $subordinate)
                include '{{$subordinate['src']}}';
            @endforeach
        @endif
        });

    @endforeach
@endif
@if ( isset($route_prefix) )
    });
@endif
