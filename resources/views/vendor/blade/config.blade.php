@if ( isset($masters) )
    // Основные сущности
    'masters' =>
    [
@foreach($masters as $master)
        '{{$master}}',
@endforeach
    ],
@endif

@if  ( isset($subordinates) )
    // Подчененные сущности
    'subordinates' =>
    [
@foreach($subordinates as $key=>$subordinate)
        '{{$key}}' =>
        [
@foreach($subordinate as $s)
            '{{$s['name']}}',
@endforeach
        ],
@endforeach
    ],
@endif
@if ( isset($options) )
    'options' =>
    [
@foreach( $options as $key=>$value )
'{{$key}}' => @if ( $value==1 ) {{$value}}, @else '{{$value}}', @endif
@endforeach

    ],
@endif