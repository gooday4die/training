Route::group([
    'prefix' => '{{ $prefix }}',
    'as' => '{{ $alias }}',
    'namespace' => '{{ $namespace }}',
    @if(!is_null($groupNamespace))'namespace' => '{{ $groupNamespace }}'
    @endif
], function () {

    Route::get('',             ['as' => 'index',  'uses' => '{{ $nameController }}@index']);

    Route::get('create',       ['as' => 'create', 'uses' => '{{ $nameController }}@create']);
    Route::post('save',        ['as' => 'save',   'uses' => '{{ $nameController }}@save']);

    Route::get('edit/{id}',    ['as' => 'edit',   'uses' => '{{ $nameController }}@edit']);
    Route::any('update/{id}',  ['as' => 'update', 'uses' => '{{ $nameController }}@update']);

    Route::any('delete/{id}',  ['as' => 'delete', 'uses' => '{{ $nameController }}@delete']);

});