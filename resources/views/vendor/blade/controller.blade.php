namespace {{ ucfirst($namespace) }};

use App\Http\Requests;
use App\Http\Controllers\Controller;

use {{ ucfirst($model_path) }};
use Illuminate\Http\Request;

class {{ $class }} extends Controller {

    public function index()
    {
        ${{ $var }} = {{ $model_name_class }}::orderBy('id', 'desc')->paginate(10);

        return view('{{ $view_path }}.index', [
            '{{ $var }}' => ${{ $var }}
        ]);
    }


    public function create()
    {
        return view('{{ $view_path }}.create');
    }


    public function save(Request $request)
    {
        ${{ $var }} = new {{ $model_name_class }}($request->all());

        ${{ $var }}->save();

        return redirect()->route('{{ $var }}.index');
    }


    public function edit($id)
    {
        ${{ $var }} = {{ $model_name_class }}::find($id);

        return view('{{ $view_path }}.edit', [
            '{{ $var }}' => ${{ $var }}
        ]);
    }


    public function update(Request $request, $id)
    {
        ${{ $var }} = {{ $model_name_class }}::find($id);

        ${{ $var }}->update($request->all());

        return redirect()->route('{{ $var }}.index');
    }


    public function delete($id)
    {
        ${{ $var }} = {{ $model_name_class }}::where('id', '=', $id);
        ${{ $var }}->delete();

        return redirect()->back();
    }

}